import express, { Request, Response } from 'express'

const server = express()

server.get('/', (req: Request, res: Response) => {
    res.send('OK:4')
})

const PORT = process.env.PORT ?? 3000

server.listen(PORT, () => {
    console.log('Server listening port', PORT)
})