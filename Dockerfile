# The build stage
FROM node:21-alpine AS builder

COPY ./*.json ./
COPY ./src ./src

RUN npm ci
RUN npm run build

# The final stage
FROM node:21-alpine AS final
WORKDIR /app
COPY --from=builder ./dist ./dist
COPY ./package*.json ./
RUN npm ci --omit=dev

EXPOSE 3000
CMD ["npm", "start"]